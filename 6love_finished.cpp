#include <iostream>
#include <string>
#include <cmath>
#include <chrono>
#include <sys/time.h>
#include <ctime>
#include <cstring>

using namespace std;

class SixLove {
    public:
        string name;
        int hint, hintDeduction, score = 20;
        int gameBoard[6][6] = {};
        int completedBoard[6][6] = {
            2,6,5,3,1,4,
            1,3,4,2,6,5,
            4,1,3,5,2,6,
            5,2,6,1,4,3,
            3,4,1,6,5,2,
            6,5,2,4,3,1
        };
        // int completedBoard[6][6]= {};
        bool isSafe(int row, int col, int num);
        void generateCompletedBoard();
        void displayBoard(int board[6][6]);
        void clearBoard(int mode);
        void selectGameMode(int mode);
        void displayHint();
        int updateCell(int cell, int value);
        bool gameOver(bool playerWon);
        void resetGame();
        bool checkBoard();
};

bool SixLove::isSafe(int row, int col, int num) {
    cout << "\nRow: " + std::to_string(row);
    cout << "\nCol: " + std::to_string(col);
    cout << "\nNum: " + std::to_string(num);
    cout << "\n";
    // Check if this number already exist with in this row or column.
    for(int i=0; i<6; i++) {
        if (completedBoard[row][i] == num || completedBoard[i][col] == num) {
            cout << "NOT SAFE!!";
            displayBoard(completedBoard);
            return false;
        }
    }

    // num needs to be a unique value in the subset/grid
    if (row == 0 || row == 1) {
        if (0 <= col && col < 3) {
            // i=row, k=column
            for (int i=0; i<2; i++) {
                for (int k=0; k<3; k++) {
                    if (completedBoard[i][k] == num) {
                        cout << "NOT SAFE1!!";
                        displayBoard(completedBoard);
                        return false;
                    }
                }
            }
        } else if (3 <= col && col < 6) {
            // i=row, k=column
            for (int i=0; i<2; i++) {
                for (int k=3; k<6; k++) {
                    if (completedBoard[i][k] == num) {
                        cout << "NOT SAFE2!!";
                        displayBoard(completedBoard);
                        return false;
                    }
                }
            }
        }
    } else if (row == 2 || row == 3) {
        if (0 <= col && col < 3) {
            // i=row, k=column
            for (int i=2; i<4; i++) {
                for (int k=0; k<3; k++) {
                    if (completedBoard[i][k] == num) {
                        cout << "NOT SAFE3!!";
                        displayBoard(completedBoard);
                        return false;
                    }
                }
            }
        } else if (3 <= col && col < 6) {
            // i=row, k=column
            for (int i=2; i<4; i++) {
                for (int k=3; k<6; k++) {
                    if (completedBoard[i][k] == num) {
                        cout << "NOT SAFE4!!";
                        displayBoard(completedBoard);
                        return false;
                    }
                }
            }
        }
    } else if (row == 4 || row == 5) {
        if (0 <= col && col < 3) {
            // i=row, k=column
            for (int i=4; i<6; i++) {
                for (int k=0; k<3; k++) {
                    if (completedBoard[i][k] == num) {
                        cout << "NOT SAFE5!!";
                        displayBoard(completedBoard);
                        return false;
                    }
                }
            }
        } else if (3 <= col && col < 6) {
            // i=row, k=column
            for (int i=4; i<6; i++) {
                for (int k=3; k<6; k++) {
                    if (completedBoard[i][k] == num) {
                        cout << "NOT SAFE6!!";
                        displayBoard(completedBoard);
                        return false;
                    }
                }
            }
        }
    }

    completedBoard[row][col] = num;
    cout << "SAFE!!";
    displayBoard(completedBoard);
    return true;
}

// Generate hidden completed board
void SixLove::generateCompletedBoard() {    
    // For this solution we will use 3x2 subset/grid method. Each subset/grid needs to be completed in order, while still obeying the other rules of sudoku (SixLove)
    int num = 0;
    auto ms = std::chrono::duration_cast< std::chrono::milliseconds >(
                    std::chrono::system_clock::now().time_since_epoch()
                ).count();

    for (int i=0; i<2; i++) {
        for(int row=0; row<2; row++) {
            if (i == 0) {
                // Left side:
                for(int col=0; col<3; col++) {
                    do {
                        ms++;
                        cout << ms;
                        srand(ms);
                        num = rand() % 6 + 1;
                    } while (!isSafe(row, col, num));
                }
            } else {
                // Right side:
                for(int col=3; col<6; col++) {
                    cout << col;
                    do {
                        ms++;
                        cout << ms;
                        srand(ms);
                        num = rand() % 6 + 1;
                    } while (!isSafe(row, col, num));
                }
            }
        }
    }

    for (int i=0; i<2; i++) {
        for(int row=2; row<4; row++) {
            if (i == 0) {
                // Left side:
                for(int col=0; col<3; col++) {
                    do {
                        ms++;
                        cout << ms;
                        srand(ms);
                        num = rand() % 6 + 1;
                    } while (!isSafe(row, col, num));
                }
            } else {
                // Right side:
                for(int col=3; col<6; col++) {
                    do {
                        ms++;
                        cout << ms;
                        srand(ms);
                        num = rand() % 6 + 1;
                    } while (!isSafe(row, col, num));
                }
            }
        }
    }

    for (int i=0; i<2; i++) {
        for(int row=4; row<6; row++) {
            if (i == 0) {
                // Left side:
                for(int col=0; col<3; col++) {
                    do {
                        ms++;
                        cout << ms;
                        srand(ms);
                        num = rand() % 6 + 1;
                    } while (!isSafe(row, col, num));
                }
            } else {
                // Right side:
                for(int col=3; col<6; col++) {
                    do {
                        ms++;
                        cout << ms;
                        srand(ms);
                        num = rand() % 6 + 1;
                    } while (!isSafe(row, col, num));
                }
            }
        }
    }
}

// Output completed game board. THIS IS SOLELY FOR DEV/TESTING.
void SixLove::displayBoard(int board[6][6]) {
    cout << "\n";
    for(int row=0; row<6; row++) {
        for(int col=0; col<6; col++) {
            cout << board[row][col];
            cout << "\t";
        }
        cout << "\n";
    }
    cout << "\n";
}

// Clear cells of completedBoard based on user game mode
void SixLove::clearBoard(int mode) {
    int row;
    int col;
    auto ms = std::chrono::duration_cast< std::chrono::milliseconds >(
                    std::chrono::system_clock::now().time_since_epoch()
                ).count();

    for (int i=0; i<6; i++) {
        for (int k=0; k<6; k++) {
            gameBoard[i][k] = completedBoard[i][k];
        }
    }

    switch(mode) {
        case 1  :
            // Remove 10 cells
            for (int i=0; i<10; i++) {
                // We need this to run at least once, but keep trying if the cell location is already 0
                // We'll set the cell to 0 afterwards, since it would mean the cell isn't already 0
                do {
                    ms++;
                    srand(ms);
                    row = rand() % 6;
                    col = rand() % 6;
                } while(gameBoard[row][col] == 0);

                gameBoard[row][col] = 0;
            }
            break;
        case 2  :
            // Remove 15 cells
            for (int i=0; i<15; i++) {
                // We need this to run at least once, but keep trying if the cell location is already 0
                // We'll set the cell to 0 afterwards, since it would mean the cell isn't already 0
                do {
                    ms++;
                    srand(ms);
                    row = rand() % 6;
                    col = rand() % 6;
                } while(gameBoard[row][col] == 0);

                gameBoard[row][col] = 0;
            }
            break;
        case 3  :
            // Remove 20 cells
            for (int i=0; i<20; i++) {
                // We need this to run at least once, but keep trying if the cell location is already 0
                // We'll set the cell to 0 afterwards, since it would mean the cell isn't already 0
                do {
                    ms++;
                    srand(ms);
                    row = rand() % 6;
                    col = rand() % 6;
                } while(gameBoard[row][col] == 0);

                gameBoard[row][col] = 0;
            }
            break;
        default :
            for (int i=0; i<10; i++) {
                // We need this to run at least once, but keep trying if the cell location is already 0
                // We'll set the cell to 0 afterwards, since it would mean the cell isn't already 0
                do {
                    ms++;
                    srand(ms);
                    row = rand() % 6;
                    col = rand() % 6;
                } while(gameBoard[row][col] == 0);

                gameBoard[row][col] = 0;
            }
            break;
    }
}

void SixLove::selectGameMode(int mode) {
    switch (mode) {
        case 1:
            hint = 1;
            hintDeduction = pow(2, hint);
            break;
        case 2:
            hint = 2;
            hintDeduction = pow(2, hint);
            break;
        case 3:
            hint = 3;
            hintDeduction = pow(2, hint);
            break;
        default:
            hint = 1;
            hintDeduction = pow(2, hint);
            break;
    }
}

void SixLove::displayHint() {
    if (hint == 0) {
        gameOver(false);
    } else {
        hint--; // Reduce hint by 1
        score = score - hintDeduction; // Reduce score by hint deduction amount

        // Find a cell that's 0
        for (int row=0; row<6; row++) {
            for (int col=0; col<6; col++) {
                if (gameBoard[row][col] == 0) {
                    cout << "Your hint is cell: " << (row*6)+col+1 << " with value: " << completedBoard[row][col] << endl
                         << "Remaining number of hints are: " << hint << endl;
                    return;
                }
            }
        }
    }
}

int SixLove::updateCell(int cell, int value) {
    // Keep track of the cells that the user has to fill. This will be used to 
    // Return 0 for a cell that isn't to be filled, 1 for successful update
    // Get row and column for board
    int row = (cell-1) / 6;
    int col = (cell-1) % 6;

    cout  << "Quotient = " << row << endl
          << "Remainder = " << col << endl;

    if (gameBoard[row][col] == 0) {
        if (value != 0) {
            // Check if the value already exist in row
            for (int i=0; i<6; i++) {
                gameBoard[row][i] == value;
                cout << "This value can't be placed here, please choose another one" << endl;
                return 0;
            }

            // Check if the value already exist in column
            for (int i=0; i<6; i++) {
                gameBoard[i][col] == value;
                cout << "This value can't be placed here, please choose another one" << endl;
                return 0;
            }
        }

        // Update cell with value
        gameBoard[row][col] = value;

        // Check if that was the last 0 cell
        for (int row=0; row<6; row++) {
            for (int col=0; col<6; col++) {
                if (gameBoard[row][col] == 0) {
                    return 1;
                }
            }
        }
        return 2;
    } else {
        cout << "This cell is already filled, please choose another one" << endl;
        return 0;
    }
}

bool SixLove::gameOver(bool playerWon) {
    string playAgain;
    if (playerWon) {
        displayBoard(gameBoard);
        cout << "Congratulations " << name << ", you won!" << endl
             << "Your score is: " << score << "\n" << endl;
    } else {
        cout << "Game Over" << endl
             << "The following board was the solution:" << endl;
        displayBoard(completedBoard);
        cout << "Your score is: 0\n" << endl;
    }

    cout << "Would you like to play another game? Enter yes to play again or no to quit: ";
    cin >> playAgain;

    while (playAgain != "yes" && playAgain != "no") {
        cout << "\n The value entered was not yes or no." << endl
             << "Would you like to play another game? Enter yes to play again or no to quit: ";
        cin >> playAgain;
    }

    resetGame();
    return playAgain == "yes" ? true : false;
}

void SixLove::resetGame() {
    name = "";
    score = 20;

    for (int row=0; row<6; row++) {
        for (int col=0; col<6; col++) {
            gameBoard[row][col] = 0;
        }
    }
}

bool SixLove::checkBoard() {
    bool playerWon = true;
    for (int row=0; row<6; row++) {
        for (int col=0; col<6; col++) {
            if (gameBoard[row][col] != completedBoard[row][col]) {
                playerWon = false;
            }
        }
    }
    return playerWon;
}

bool isNumber(string s)
{
    for (int i = 0; i < s.length(); i++)
        if (isdigit(s[i]) == false)
            return false;
 
    return true;
}

int main()
{
    bool play = true;

    string selectedCell, selectedMode, cellValue;
    string name;

    SixLove sixLove;
    // sixLove.generateCompletedBoard();
    // sixLove.displayBoard(sixLove.completedBoard);

    while (play == true) {
        cout << "Please enter your player name: ";
        cin >> name;

        sixLove.name = name;

        // Doesn't matter what the user enters here as their name

        cout << endl
            << "\n\nHello " << name << "! Welcome to SixLove.\n\n"
            << "Your objective is to guess the correct number for each cell that is marked by a 0." << endl
            << "You can select the cell you will like to fill by entering the number of the cell\n(cells are numbered 1 - 36, the numbers increase from left to right)\n";

        cout << "\nPlease enter the level you want to play (enter the number):" << "\n"
             << "1. Rookie" << endl
             << "2. Tuff Gong" << endl
             << "3. Hard Seed" << "\n\n";

        cout << "Level: ";
        cin >> selectedMode;

        while (!isNumber(selectedMode)) {
            cout << "\n The value entered was not a number. Please enter a number: ";
            cin >> selectedMode;
        }

        sixLove.selectGameMode(std::stoi(selectedMode));
        sixLove.clearBoard(std::stoi(selectedMode));

        // Display the board that the user has to fill
        cout << "\n\nHere are the rules for the selected game mode:" << endl
             << "You are allowed to request " << sixLove.hint;

             if (sixLove.hint == 1) {
                cout << " hint, by entering h at any point during the game." << endl;
             } else {
                cout << " hints, by entering h or H at any point during the game." << endl;
             }

             cout << "Each time you request a hint, " << sixLove.hintDeduction << " points shall be deducted from your overall score" << endl
                  << "If you wish to exit the game, enter q." << endl
                  << "\nBelow is the board you have to complete:";

        bool boardCompleted = false;

        do {
            sixLove.displayBoard(sixLove.gameBoard);

            // Cell
            cout << "Select a cell to update: ";
            cin >> selectedCell;

            if (selectedCell == "q") {
                play = sixLove.gameOver(false);
                boardCompleted = true;
            } else if (selectedCell == "h") {
                // Display hint
                sixLove.displayHint();
            } else {
                while (!isNumber(selectedCell)) {
                    cout << "\n The value entered was not a number. Please enter a number: ";
                    cin >> selectedCell;
                }

                // Cell Value
                cout << "Enter a value to fill cell " << selectedCell << " with: ";
                cin >> cellValue;

                if (cellValue == "q") {
                    play = sixLove.gameOver(false);
                    boardCompleted = true;
                } else if (cellValue == "h") {
                    // Display hint
                    sixLove.displayHint();
                } else {
                    while (!isNumber(cellValue)) {
                        cout << "\n The value entered was not a number. Please enter a number: ";
                        cin >> cellValue;
                    }
                }

                int cellUpdateResult = sixLove.updateCell(std::stoi(selectedCell), std::stoi(cellValue));

                if (cellUpdateResult == 2) {
                    // Board is "completed", check if it's completed properly
                    if (sixLove.checkBoard()) {
                        // Game is completed correctly, therefore end game
                        play = sixLove.gameOver(true);
                        boardCompleted = true;
                    } else {
                        // Game is completed incorrectly, therefore end game
                        play = sixLove.gameOver(false);
                        boardCompleted = true;
                    }
                }
            }
        } while(!boardCompleted);
    }

}